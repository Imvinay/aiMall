'use strict';

// App Module: the name aiMall matches the ng-app attribute in the main <html> tag
// the route provides parses the URL and injects the appropriate partial page
var storeApp = angular.module('aiMall', []).
  config(['$routeProvider', function($routeProvider) {
  $routeProvider.
      when('/firstPage', {
        templateUrl: 'partials/firstPage.htm',
        controller: loginController 
      }).
      when('/secondPage', {
        templateUrl: 'partials/secondPage.htm',
        controller: loginController
      }).
        when('/content', {
            templateUrl: 'partials/content.htm',
            controller: loginController
        }).
        when('/interactions', {
            templateUrl: 'partials/interactions.htm',
            controller: loginController
        }).
        when('/register', {
            templateUrl: 'partials/register.htm',
            controller: loginController
        }).
        when('/registerConfirmation', {
            templateUrl: 'partials/registerConfirmation.htm',
            controller: loginController
        }).
        when('/addAccount', {
            templateUrl: 'partials/addAccount.htm',
            controller: loginController
        }).
      otherwise({
          redirectTo: '/firstPage'
      });
}]);

// create a data service that provides a store and a shopping cart that
// will be shared by all views (instead of creating fresh ones for each view).
storeApp.factory("DataService", function () {

    // create store
    var myStore = new store();

    // create shopping cart
    var myCart = new shoppingCart("aiMall");

    // enable PayPal checkout
    // note: the second parameter identifies the merchant; in order to use the 
    // shopping cart with PayPal, you have to create a merchant account with 
    // PayPal. You can do that here:
    // https://www.paypal.com/webapps/mpp/merchant
    myCart.addCheckoutParameters("PayPal", "bernardo.castilho-facilitator@gmail.com");

    // enable Google Wallet checkout
    // note: the second parameter identifies the merchant; in order to use the 
    // shopping cart with Google Wallet, you have to create a merchant account with 
    // Google. You can do that here:
    // https://developers.google.com/commerce/wallet/digital/training/getting-started/merchant-setup
    myCart.addCheckoutParameters("Google", "500640663394527",
        {
            ship_method_name_1: "UPS Next Day Air",
            ship_method_price_1: "20.00",
            ship_method_currency_1: "USD",
            ship_method_name_2: "UPS Ground",
            ship_method_price_2: "15.00",
            ship_method_currency_2: "USD"
        }
    );

    // return data object with store and cart
    return {
        store: myStore,
        cart: myCart
    };
});